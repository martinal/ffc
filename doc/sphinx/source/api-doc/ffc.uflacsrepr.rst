ffc.uflacsrepr package
======================

Submodules
----------

ffc.uflacsrepr.uflacsgenerator module
-------------------------------------

.. automodule:: ffc.uflacsrepr.uflacsgenerator
    :members:
    :undoc-members:
    :show-inheritance:

ffc.uflacsrepr.uflacsoptimization module
----------------------------------------

.. automodule:: ffc.uflacsrepr.uflacsoptimization
    :members:
    :undoc-members:
    :show-inheritance:

ffc.uflacsrepr.uflacsrepresentation module
------------------------------------------

.. automodule:: ffc.uflacsrepr.uflacsrepresentation
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ffc.uflacsrepr
    :members:
    :undoc-members:
    :show-inheritance:
