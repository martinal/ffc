ffc.backends package
====================

Subpackages
-----------

.. toctree::

    ffc.backends.dolfin
    ffc.backends.ufc

Module contents
---------------

.. automodule:: ffc.backends
    :members:
    :undoc-members:
    :show-inheritance:
