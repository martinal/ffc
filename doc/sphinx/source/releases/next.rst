Changes in the next release of FFC
==================================

- Generalize ufc interface to non-affine parameterized coordinates
- Add ``ufc::coordinate_mapping`` class
- Make ufc interface depend on C++11 features requiring gcc version >= 4.8
